package unit.shop;

import shop.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.*;

public class StandardItemTest {
    private static int id;
    private static String name;
    private static float price;
    private static String category;
    private static int loyaltyPoints;
    private static StandardItem item;

    @BeforeAll
    public static void setUp() {
        id = 123;
        name = "Java for Dummies";
        price = 149.99f;
        category = "Books";
        loyaltyPoints = 1000;

        item = new StandardItem(id, name, price, category, loyaltyPoints);
    }

    @Test
    public void constructor_CorrectlyCreated() {
        assertEquals(id, item.getID());
        assertEquals(name, item.getName());
        assertEquals(price, item.getPrice());
        assertEquals(category, item.getCategory());
        assertEquals(loyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    public void copy_CorrectlyCopied() {
        StandardItem copy = item.copy();

        assertNotSame(item, copy);
        assertEquals(id, copy.getID());
        assertEquals(name, copy.getName());
        assertEquals(price, copy.getPrice());
        assertEquals(category, copy.getCategory());
        assertEquals(loyaltyPoints, copy.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "123, Java for Dummies, 149.99, Books, 1000, true",
            "100, Java for Dummies, 149.99, Books, 1000, false",
            "123, Java FOR Dummies, 149.99, Books, 1000, false",
            "123, Java for Dummies, 49.99, Books, 1000, false",
            "123, Java for Dummies, 149.99, CDs, 1000, false",
            "123, Java for Dummies, 149.99, Books, 100, false"
    })
    public void equals_CorrectComparison(int id, String name, float price, String category, int loyaltyPoints, boolean isEqual) {
        StandardItem compareItem = new StandardItem(id, name, price, category, loyaltyPoints);

        boolean result = item.equals(compareItem);

        assertEquals(isEqual, result);
    }
}
