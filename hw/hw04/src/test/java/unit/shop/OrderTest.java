package unit.shop;

import shop.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {
    private static ArrayList<Item> items;
    private static ShoppingCart cart;
    private static String customerName;
    private static String customerAddress;
    private static int state;

    @BeforeAll
    public static void setUp() {
        items = new ArrayList<>(Arrays.asList(
                new StandardItem(1, "Item 1", 100, "Items", 100),
                new StandardItem(2, "Items 2", 200, "Items", 200),
                new DiscountedItem(3, "Items 3", 300, "Items", 50, "1.1.2001", "1.1.2001")
        ));
        cart = new ShoppingCart(items);
        customerName = "Vaclav Smitka";
        customerAddress = "Karlovo nam. 13, E-307";
        state = 100;
    }

    @Test
    public void constructor_CorrectValuesWithState() {
        Order order = new Order(cart, customerName, customerAddress, state);

        assertEquals(items, order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }

    @Test
    public void constructor_NullValueWithState() {
        Order order = new Order(null, customerName, customerAddress, state);

        assertNotNull(order);
        assertTrue(order.getItems().isEmpty());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }

    @Test
    public void constructor_CorrectValuesWithoutState() {
        Order order = new Order(cart, customerName, customerAddress);

        assertEquals(items, order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    public void constructor_NullValueWithoutState() {
        Order order = new Order(null, customerName, customerAddress);

        assertNotNull(order);
        assertTrue(order.getItems().isEmpty());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }
}
