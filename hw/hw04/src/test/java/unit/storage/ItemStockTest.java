package unit.storage;

import storage.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {
    private static StandardItem item;
    private static ItemStock stock;

    @BeforeEach
    public void setUp() {
        item = new StandardItem(123, "Java for Dummies", 149.99f, "Books", 1000);
        stock = new ItemStock(item);
    }

    @Test
    public void constructor_CorrectlyCreated() {
        assertEquals(item, stock.getItem());
        assertEquals(0, stock.getCount());
    }

    @ParameterizedTest
    @CsvSource({"0, 10",
            "5, 15",
            "-5, 5",
            "-10, 0",
            "-11, 10"})
    public void decreaseItemCount_CorrectlyDecreased(int changeValue, int expectedValue) {
        stock.increaseItemCount(10);

        if (changeValue >= 0) stock.increaseItemCount(changeValue);
        else stock.decreaseItemCount(-changeValue);

        assertEquals(expectedValue, stock.getCount());
    }
}
