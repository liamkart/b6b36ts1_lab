package unit.archive;

import static org.mockito.Mockito.*;

import archive.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import shop.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PurchasesArchiveTest {
    private ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry;
    private HashMap<Integer, ItemPurchaseArchiveEntry> mockedItemPurchaseArchive;
    private ArrayList<Order> mockedOrderArchive;
    private PurchasesArchive purchaseArchive;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUp() {
        mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        mockedItemPurchaseArchive = mock(HashMap.class);
        mockedOrderArchive = mock(ArrayList.class);

        purchaseArchive = new PurchasesArchive(mockedItemPurchaseArchive, mockedOrderArchive);

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void constructor_CorrectCreation() {
        Item mockedItem = mock(Item.class);

        ItemPurchaseArchiveEntry entry = new ItemPurchaseArchiveEntry(mockedItem);

        assertEquals(mockedItem, entry.getRefItem());
        assertEquals(1, entry.getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void printItemPurchaseStatistics_CorrectOutput() {
        ArrayList<ItemPurchaseArchiveEntry> entries = new ArrayList<>(Collections.singletonList(mockedItemPurchaseArchiveEntry));

        when(mockedItemPurchaseArchive.values()).thenReturn(entries);
        when(mockedItemPurchaseArchiveEntry.toString()).thenReturn("Test string");

        purchaseArchive.printItemPurchaseStatistics();

        String expected = "ITEM PURCHASE STATISTICS:\nTest string\n";

        assertEquals(expected, outContent.toString());
    }

    @ParameterizedTest
    @CsvSource({"1, 1, 100",
            "1, 2, 0"})
    public void getHowManyTimesHasBeenItemSold_CorrectBehaviour(int validId, int itemId, int expected) {
        Item mockedItem = mock(Item.class);

        when(mockedItem.getID()).thenReturn(itemId);
        when(mockedItemPurchaseArchive.get(validId)).thenReturn(mockedItemPurchaseArchiveEntry);
        when(mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(expected);
        if (validId == itemId) when(mockedItemPurchaseArchive.containsKey(itemId)).thenReturn(true);
        else when(mockedItemPurchaseArchive.containsKey(itemId)).thenReturn(false);

        int result = purchaseArchive.getHowManyTimesHasBeenItemSold(mockedItem);

        verify(mockedItemPurchaseArchive, times(1)).containsKey(itemId);
        if (validId == itemId) {
            verify(mockedItemPurchaseArchive,times(1)).get(itemId);
            verify(mockedItemPurchaseArchiveEntry,times(1)).getCountHowManyTimesHasBeenSold();
        } else {
            verify(mockedItemPurchaseArchive,times(0)).get(itemId);
            verify(mockedItemPurchaseArchiveEntry,times(0)).getCountHowManyTimesHasBeenSold();
        }

        assertEquals(expected, result);
    }

    @ParameterizedTest
    @CsvSource({"1, 1",
            "1, 2"})
    public void putOrderToPurchasesArchive_CorrectBehaviour(int validId, int itemId) {
        Order mockedOrder = mock(Order.class);
        Item mockedItem = mock(Item.class);

        ArrayList<Item> items = new ArrayList<>(Collections.singletonList(mockedItem));

        when(mockedItem.getID()).thenReturn(itemId);
        when(mockedOrder.getItems()).thenReturn(items);
        when(mockedItemPurchaseArchive.get(validId)).thenReturn(mockedItemPurchaseArchiveEntry);
        if (validId == itemId) when(mockedItemPurchaseArchive.containsKey(itemId)).thenReturn(true);
        else when(mockedItemPurchaseArchive.containsKey(itemId)).thenReturn(false);

        purchaseArchive.putOrderToPurchasesArchive(mockedOrder);

        verify(mockedOrderArchive, times(1)).add(mockedOrder);
        verify(mockedOrder, times(1)).getItems();
        verify(mockedItemPurchaseArchive, times(1)).containsKey(itemId);
        if (validId == itemId) {
            verify(mockedItemPurchaseArchive, times(1)).get(itemId);
            verify(mockedItemPurchaseArchiveEntry, times(1)).increaseCountHowManyTimesHasBeenSold(1);
        } else {
            verify(mockedItemPurchaseArchive, times(1)).put(Mockito.anyInt(), Mockito.any(ItemPurchaseArchiveEntry.class));
        }
    }
}
