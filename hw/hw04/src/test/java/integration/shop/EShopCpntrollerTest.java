package integration.shop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import shop.*;
import storage.*;
import archive.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

public class EShopCpntrollerTest {
    private static Storage storage;
    private static PurchasesArchive archive;
    private static ArrayList<ShoppingCart> carts;
    private static ArrayList<Order> orders;
    private static ArrayList<Item> items;

    @BeforeAll
    public static void setUp() {
        EShopController.startEShop();
        storage = EShopController.getStorage();
        archive = EShopController.getArchive();
        carts = EShopController.getCarts();
        orders = EShopController.getOrders();

        // Creating shop items

        items = new ArrayList<>(Arrays.asList(
                new StandardItem(1, "Java for Dummies", 149.99f, "Books", 100),
                new StandardItem(2, "iPhone 4S", 1499.99f, "Phones", 1500),
                new StandardItem(3, "Rohlik", 2.90f, "Food", 0),
                new DiscountedItem(4, "Kozel 11", 14.99f, "Drinks", 30, "1.4.2024", "1.6.2024"),
                new DiscountedItem(5, "FEL OI T-Shirt", 109.99f, "Clothes", 10, "1.1.2024", "1.1.2034")
        ));

        for (Item item : items) storage.insertItems(item, 10);

        Collection<ItemStock> stock = storage.getStockEntries();
        Iterator<ItemStock> stockIterator = stock.iterator();

        assertEquals(5, stock.size());

        for (int i = 0; i < stock.size(); ++i) {
            Item item = items.get(i);
            Item itemInStorage = stockIterator.next().getItem();

            assertEquals(10, storage.getItemCount(itemInStorage));
            assertEquals(item, itemInStorage);
        }
    }

    @Test
    public void makeOrder_NormalBehaviour() {
        // Create shopping cart

        ShoppingCart cart = EShopController.newCart();

        assertEquals(1, carts.size());
        assertEquals(0, cart.getItemsCount());

        Item item = items.get(0);

        // Adding item

        cart.addItem(item);
        assertEquals(1, cart.getItemsCount());
        assertEquals(item, cart.getCartItems().get(0));

        // Purchase cart

        try {
            EShopController.purchaseShoppingCart(cart, "Vaclav Smitka","Karlovo nam. 13, E-307");
        } catch (NoItemInStorage ignored) {}

        assertEquals(9, storage.getItemCount(item.getID()));
        assertEquals(1, archive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    public void makeOrder_EmptyCart() {
        // Create shopping cart

        ShoppingCart cart = EShopController.newCart();

        assertEquals(1, carts.size());
        assertEquals(0, cart.getItemsCount());

        Item item = items.get(1);

        // Adding item

        cart.addItem(item);
        assertEquals(1, cart.getItemsCount());
        assertEquals(item, cart.getCartItems().get(0));

        // Checking price

        float totalPrice = cart.getTotalPrice();

        assertEquals(1499.99f, totalPrice);

        // Removing item

        cart.removeItem(item.getID());
        assertEquals(0, cart.getItemsCount());

        // Purchase empty cart (try to)

        try {
            EShopController.purchaseShoppingCart(cart, "Vaclav Smitka","Karlovo nam. 13, E-307");
        } catch (NoItemInStorage ignored) {}

        assertEquals(0, orders.size());
        assertEquals(10, storage.getItemCount(item.getID()));
        assertEquals(0, archive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    public void makeOrder_NotEnoughItemsAtStorage() {
        // Create shopping cart

        ShoppingCart cart = EShopController.newCart();

        assertEquals(1, carts.size());
        assertEquals(0, cart.getItemsCount());

        Item item = items.get(2);

        // Adding item (too much)

        for (int i = 0; i < 11; ++i) cart.addItem(item);

        assertEquals(11, cart.getItemsCount());
        assertEquals(item, cart.getCartItems().get(0));

        // Purchase empty cart (try to)

        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(cart, "Vaclav Smitka","Karlovo nam. 13, E-307");
        });

        assertEquals(0, orders.size());
        assertEquals(10, storage.getItemCount(item.getID()));
        assertEquals(0, archive.getHowManyTimesHasBeenItemSold(item));
    }
}
