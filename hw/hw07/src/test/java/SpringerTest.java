import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;

public class SpringerTest {
    private static WebDriver driver = new ChromeDriver();

    @BeforeAll
    public static void logIn() {
        HomePage homePage = new HomePage(driver).openPage();
        homePage.clickLogin()
                .fillEmail("seleniumtesteremail@email.cz")
                .clickContinue()
                .fillPassword("seleniumtesterpassword")
                .submitLogIn();
    }

    @AfterAll
    public static void closeDriver() {
        driver.close();
    }

    private static List<ArticlePage> articlesSaver() {
        SearchPage searchPage = new SearchPage(driver).openPage();
        return searchPage
                .goToOldSearch()
                .clickSearchOptions()
                .goToAdvancedSearch()
                .fillAllWords("Page Object Model")
                .fillSomeWords("Sellenium Testing")
                .submitSearch()
                .chooseArticles()
                .clickDateChoice()
                .selectDateSearchMode("in")
                .fillStartDate("2024")
                .submitDateChoice()
                .getResults(4);
    }

    @ParameterizedTest
    @MethodSource("articlesSaver")
    public void searchingTest(ArticlePage expectedPage){
        String title = expectedPage.getArticleTitle();
        HomePage homePage = new HomePage(driver).openPage();
        ArticlePage resultPage = homePage
                .fillSearch(title)
                .submitSearch()
                .getFirstResult()
                .openPage();
        Assertions.assertEquals(expectedPage.getPublicationDate(), resultPage.getPublicationDate());
        Assertions.assertEquals(expectedPage.getDoi(), resultPage.getDoi());
    }
}
