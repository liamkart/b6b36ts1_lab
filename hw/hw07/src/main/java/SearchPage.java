
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url = "https://link.springer.com/search";

    @FindBy(css = "button[data-cc-action='accept']")
    private WebElement acceptAllCookiesButton;

    @FindBy(how = How.CSS, using = "a[data-test='core search link']")
    private WebElement oldSearchLink;

    @FindBy(className = "open-search-options")
    private WebElement searchOptionsButton;

    @FindBy(id = "advanced-search-link")
    private WebElement advancedSearchLink;

    @FindBy(xpath = "//a[@class='facet-link']/span[@class='facet-title'][text()='Article']")
    private WebElement articleChoice;

    @FindBy(css = "button.expander-title")
    private WebElement datePublishedButton;

    @FindBy(id = "date-facet-mode")
    private WebElement dateSearchModeSelect;

    @FindBy(css = "span.field.date-between-start input#start-year")
    private WebElement startYearInput;

    @FindBy(id = "date-facet-submit")
    private WebElement dateSubmitButton;

    @FindBy(id = "results-list")
    private WebElement oldSearchResults;

    @FindBy(className = "u-list-reset")
    private WebElement newSearchResults;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public SearchPage openPage() {
        this.driver.get(url);
        try {acceptAllCookiesButton.click();}
        catch (Exception ignored) {}
        return this;
    }

    public SearchPage goToOldSearch() {
        wait.until(ExpectedConditions.visibilityOf(oldSearchLink));
        oldSearchLink.click();
        return this;
    }

    public SearchPage clickSearchOptions() {
        wait.until(ExpectedConditions.visibilityOf(searchOptionsButton));
        searchOptionsButton.click();
        return this;
    }

    public AdvancedSearchPage goToAdvancedSearch() {
        wait.until(ExpectedConditions.visibilityOf(advancedSearchLink));
        advancedSearchLink.click();
        return new AdvancedSearchPage(driver);
    }

    public SearchPage chooseArticles() {
        wait.until(ExpectedConditions.visibilityOf(articleChoice));
        articleChoice.click();
        return this;
    }

    public SearchPage clickDateChoice() {
        wait.until(ExpectedConditions.visibilityOf(datePublishedButton));
        datePublishedButton.click();
        return this;
    }

    public SearchPage selectDateSearchMode(String text) {
        wait.until(ExpectedConditions.visibilityOf(dateSearchModeSelect));
        if (text.equals("in") || text.equals("between")) dateSearchModeSelect.sendKeys(text);
        else throw new InvalidArgumentException("Invalid mode '" + text + "'!");
        return this;
    }

    public SearchPage fillStartDate(String text) {
        wait.until(ExpectedConditions.visibilityOf(startYearInput));
        startYearInput.clear();
        startYearInput.sendKeys(text);
        return this;
    }

    public SearchPage submitDateChoice() {
        wait.until(ExpectedConditions.visibilityOf(dateSubmitButton));
        dateSubmitButton.click();
        return this;
    }

    public List<ArticlePage> getResults(int count) {
        List<ArticlePage> articles = new ArrayList<>();

        wait.until(ExpectedConditions.visibilityOf(oldSearchResults));
        List<WebElement> elements = oldSearchResults.findElements(By.tagName("li"));
        for (int i = 0; i < count; i++) {
            WebElement element = elements.get(i);
            String link = element.findElement(By.className("title")).getAttribute("href");
            articles.add(new ArticlePage(driver, link));
        }

        return articles;
    }

    public ArticlePage getFirstResult() {
        wait.until(ExpectedConditions.visibilityOf(newSearchResults));
        List<WebElement> elements = newSearchResults.findElements(By.tagName("li"));
        WebElement element = elements.get(0);
        String link = element.findElement(By.className("app-card-open__link")).getAttribute("href");
        return new ArticlePage(driver, link);
    }
}
