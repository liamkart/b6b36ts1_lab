import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ArticlePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url;

    @FindBy(css = "button[data-cc-action='accept']")
    private WebElement acceptAllCookiesButton;

    @FindBy(css = "h1.c-article-title[data-test='article-title']")
    private WebElement articleTitle;

    @FindBy(css = "li.c-article-identifiers__item time")
    private WebElement publicationDate;

    private String doi;

    public ArticlePage(WebDriver driver, String url) {
        this.driver = driver;
        this.url = url;
        String[] url_parts = url.split("/");
        doi = url_parts[4] + "/" + url_parts[5];
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public ArticlePage openPage() {
        this.driver.get(url);
        try {acceptAllCookiesButton.click();}
        catch (Exception ignored) {}
        return this;
    }

    public String getArticleTitle() {
        this.driver.get(url);
        wait.until(ExpectedConditions.visibilityOf(articleTitle));
        return articleTitle.getText();
    }

    public String getPublicationDate() {
        this.driver.get(url);
        wait.until(ExpectedConditions.visibilityOf(publicationDate));
        return publicationDate.getText();
    }

    public String getDoi() {
        return doi;
    }
}
