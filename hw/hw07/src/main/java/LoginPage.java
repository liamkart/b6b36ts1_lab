import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url = "https://link.springer.com/signup-login";

    @FindBy(css = "button[data-cc-action='accept']")
    private WebElement acceptAllCookiesButton;

    @FindBy(id = "login-email")
    private WebElement emailField;

    @FindBy(id = "login-password")
    private WebElement passwordField;

    @FindBy(id = "email-submit")
    private WebElement continueButton;

    @FindBy(id = "password-submit")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public LoginPage openPage() {
        this.driver.get(url);
        try {acceptAllCookiesButton.click();}
        catch (Exception ignored) {}
        return this;
    }

    public LoginPage fillEmail(String text) {
        wait.until(ExpectedConditions.visibilityOf(emailField));
        emailField.sendKeys(text);
        return this;
    }

    public LoginPage fillPassword(String text) {
        wait.until(ExpectedConditions.visibilityOf(passwordField));
        passwordField.sendKeys(text);
        return this;
    }

    public LoginPage clickContinue() {
        wait.until(ExpectedConditions.visibilityOf(continueButton));
        continueButton.click();
        return this;
    }

    public HomePage submitLogIn() {
        wait.until(ExpectedConditions.visibilityOf(loginButton));
        loginButton.click();
        return new HomePage(driver);
    }
}
