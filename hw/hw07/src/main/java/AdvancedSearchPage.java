import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AdvancedSearchPage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url = "https://link.springer.com/advanced-search";

    @FindBy(id = "all-words")
    private WebElement allWordsField;

    @FindBy(id = "least-words")
    private WebElement someWordsField;

    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public AdvancedSearchPage openPage() {
        this.driver.get(url);
        return this;
    }

    public AdvancedSearchPage fillAllWords(String text) {
        wait.until(ExpectedConditions.visibilityOf(allWordsField));
        allWordsField.sendKeys(text);
        return this;
    }

    public AdvancedSearchPage fillSomeWords(String text) {
        wait.until(ExpectedConditions.visibilityOf(someWordsField));
        someWordsField.sendKeys(text);
        return this;
    }

    public SearchPage submitSearch(){
        searchButton.click();
        return new SearchPage(driver);
    }
}
