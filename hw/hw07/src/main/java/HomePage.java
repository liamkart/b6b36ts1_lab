import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url = "https://link.springer.com";

    @FindBy(css = "a.c-header__link#identity-account-widget")
    private WebElement loginButton;

    @FindBy(css = "button[data-cc-action='accept']")
    private WebElement acceptAllCookiesButton;

    @FindBy(id = "homepage-search")
    private WebElement searchField;

    @FindBy(className = "app-homepage-hero__button")
    private WebElement searchButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public HomePage openPage() {
        this.driver.get(url);
        try {acceptAllCookiesButton.click();}
        catch (Exception ignored) {}
        return this;
    }

    public LoginPage clickLogin() {
        wait.until(ExpectedConditions.visibilityOf(loginButton));
        loginButton.click();
        return new LoginPage(driver).openPage();
    }

    public HomePage fillSearch(String text) {
        wait.until(ExpectedConditions.visibilityOf(searchField));
        searchField.sendKeys(text);
        return this;
    }

    public SearchPage submitSearch() {
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();
        return new SearchPage(driver);
    }
}
