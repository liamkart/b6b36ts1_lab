package cz.fel.cvut.ts1;

public class Factor {
    public static long factorial_iter (int n) {
        long res = 1;
        while (n > 1) res *= n--;
        return res;
    }

    public static long factorial_recur (int n) {
        if (n <= 1) return 1;
        else return n * factorial_recur(n - 1);
    }

    public static int factorial_dumb (int n) {
        if (n < 0) return -1;
        else if (n == 0 || n == 1) return 1;
        else {
            int result = n;
            for (int i = n - 1; i >= 1; i--) result *= i;
            return result;
        }
    }
}
