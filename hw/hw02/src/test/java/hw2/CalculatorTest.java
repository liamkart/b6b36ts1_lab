package hw2;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    private static Calculator calc;

    @BeforeAll
    public static void setUpClass() {
        calc = new Calculator();
    }

    @Test
    public void add_ValidInput_CorrectResult() {
        int expected = 13;
        int result = calc.add(10, 3);
        assertEquals(expected, result);
    }

    @Test
    public void subtract_ValidInput_CorrectResult() {
        int expected = 2;
        int result = calc.subtract(8, 6);
        assertEquals(expected, result);
    }

    @Test
    public void multiply_ValidInput_CorrectResult() {
        int expected = 33;
        int result = calc.multiply(3, 11);
        assertEquals(expected, result);
    }

    @Test
    public void divide_ValidInput_CorrectResult() {
        int expected = 1;
        int result = calc.divide(13, 13);
        assertEquals(expected, result);
    }

    @Test
    public void divide_ZeroDivision_ExceptionThrown() {
        assertThrows(ArithmeticException.class, () -> {
            calc.divide(13, 0);
        });
    }
}
