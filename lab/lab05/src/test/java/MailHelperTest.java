import cz.cvut.fel.ts1.refactoring.Configuration;
import cz.cvut.fel.ts1.refactoring.DBManager;
import cz.cvut.fel.ts1.refactoring.Mail;
import cz.cvut.fel.ts1.refactoring.MailHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

public class MailHelperTest {
    @Test
    public void createAndSendMail_smth_smth() {
        DBManager dbManager = new DBManager();
        TestableMailHelper mailHelper = new TestableMailHelper(dbManager);
        int expectedCount = 1;

        mailHelper.createAndSendMail("...", "...", "...");
        Assertions.assertEquals(expectedCount, mailHelper.getSendMailCounter());
    }

    class TestableMailHelper extends MailHelper {

        private List<Mail> mailStore = new ArrayList<>();
        private int sendMailCounter = 0;

        public TestableMailHelper(DBManager dbManager) {
            super(dbManager);
        }

        @Override
        public void saveMailChanges(Mail mail) {
           mailStore.add(mail);
        }

        @Override
        public Mail findMail(int mailId) {
            for (Mail mail : mailStore) {
                if (mail.getMailId() == mailId) {
                    return mail;
                }
            }
            return null;
        }

        @Override
        public void sendMailAsync(Mail mail) {
            sendMailCounter++;
        }

        @Override
        public void sendMail(MimeMessage message) throws MessagingException {
            sendMailCounter++;
        }

        public int getSendMailCounter() {
            return sendMailCounter;
        }
    }
}
