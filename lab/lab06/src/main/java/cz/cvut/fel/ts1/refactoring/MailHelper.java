package cz.cvut.fel.ts1.refactoring;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author balikm1
 */
public class MailHelper {
    private DBManager dbManager;

    public MailHelper(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    public void createAndSendMail(String to, String subject, String body)
    {
        Mail mail = new Mail();
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setBody(body);
        mail.setIsSent(false);
        this.saveMailChanges(mail);

        sendMailAsync(mail);
    }
    
    public void sendMail(int mailId)
    {
        try
        {
            // get entity
            Mail mail = this.findMail(mailId);
            if (mail == null) {
                return;
            }

            if (mail.isSent()) {
                return;
            }

            String from = "user@fel.cvut.cz";
            String smtpHostServer = "smtp.cvut.cz";
            Properties props = System.getProperties();
            props.put("mail.smtp.host", smtpHostServer);
            Session session = Session.getInstance(props, null);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(from);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
            message.setSubject(mail.getSubject());
            message.setText(mail.getBody(), "UTF-8");

            // send
            sendMail(message);
            mail.setIsSent(true);
            this.saveMailChanges(mail);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }

    public void saveMailChanges(Mail mail) {
        this.dbManager.saveMail(mail);
    }

    public Mail findMail(int mailId) {
        return this.dbManager.findMail(mailId);
    }

    public void sendMailAsync(Mail mail) {
        if (!Configuration.isDebug) {
            (new Thread(() -> {
                sendMail(mail.getMailId());
            })).start();
        }
    }

    public void sendMail(MimeMessage message) throws MessagingException {
        Transport.send(message);
    }
}
