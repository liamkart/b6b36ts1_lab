import cz.cvut.fel.ts1.refactoring.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;

import static org.mockito.Mockito.*;

public class MainHelperMockTest {
    @Test
    public void createAndSendMail_saveMail_callOnce() {
        DBManager mockedDBManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDBManager);

        mailHelper.createAndSendMail("test@test.com", "Subject", "Body");

        verify(mockedDBManager, times(1)).saveMail(any(Mail.class));
    }

    @Test
    public void createAndSendMail_findMail_correctMail() {
        DBManager mockedDBManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDBManager);
        Mail mail = new Mail();
        int idx = 1;

        mail.setMailId(idx);

        mailHelper.sendMail(idx);

        when(mockedDBManager.findMail(idx)).thenReturn(mail);

        verify(mockedDBManager, times(1)).findMail(idx);
    }
}
