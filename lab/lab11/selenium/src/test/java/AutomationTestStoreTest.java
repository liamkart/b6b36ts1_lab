import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomationTestStoreTest {
    private WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        driver = new ChromeDriver();
    }

    @Test
    public void registrationTest(){
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        homePage.openStore();
        homePage.clickLogin();
        loginPage.clickContinue();
        registrationPage.setFirstName("Jirka");
        registrationPage.setLastName("Sebek");
        registrationPage.clickSubmit();
    }

    @Test
    public void fluentRegistration() {
        new HomePage(driver)
                .openStore()
                .clickLogin()
                .clickContinue()
                .setFirstName("Jirka")
                .setLastName("Sebek")
                .clickSubmit();
    }
}
