import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage {
    private String url = "https://automationteststore.com/index.php?rt=account/login";

    @FindBy(css = "[title='Continue']")
    private WebElement continueButton;

    private WebDriver driver;
    private WebDriverWait wait;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
    }

    public RegistrationPage clickContinue() {
        wait.until(ExpectedConditions.visibilityOf(continueButton));
        continueButton.click();
        return new RegistrationPage(driver);
    }
}
