import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class RegistrationPage {
    private String url = "https://automationteststore.com/index.php?rt=account/create";

    @FindBy(css = "#AccountFrm_firstname")
    private WebElement firstName;

    @FindBy(css = "#AccountFrm_lastname")
    private WebElement lastName;

    @FindBy(css = "[type='submit']")
    private WebElement submitButton;

    private WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public RegistrationPage setFirstName(String firstName) {
        this.firstName.sendKeys(firstName);
        return this;
    }

    public RegistrationPage setLastName(String lastName) {
        this.lastName.sendKeys(lastName);
        return this;
    }

    public void clickSubmit() {
        submitButton.click();
    }
}
