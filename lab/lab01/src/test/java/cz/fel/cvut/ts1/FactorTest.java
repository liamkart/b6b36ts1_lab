package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FactorTest {
    @Test
    public void factorialZeroIterative() {
        long expected = 1;
        long actual = Factor.factorial_iter(0);
        assertEquals(expected, actual);
    }

    @Test
    public void factorialOneIterative() {
        long expected = 1;
        long actual = Factor.factorial_iter(1);
        assertEquals(expected, actual);
    }

    @Test
    public void factorialFiveIterative() {
        long expected = 2 * 3 * 4 * 5;
        long actual = Factor.factorial_iter(5);
        assertEquals(expected, actual);
    }

    @Test
    public void factorialZeroRecursive() {
        long expected = 1;
        long actual = Factor.factorial_recur(0);
        assertEquals(expected, actual);
    }

    @Test
    public void factorialOneRecursive() {
        long expected = 1;
        long actual = Factor.factorial_recur(1);
        assertEquals(expected, actual);
    }

    @Test
    public void factorialFiveRecursive() {
        long expected = 2 * 3 * 4 * 5;
        long actual = Factor.factorial_recur(5);
        assertEquals(expected, actual);
    }
}
