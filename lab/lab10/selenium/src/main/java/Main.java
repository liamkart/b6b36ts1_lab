import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

public class Main {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();

        driver.get("https://ts1.v-sources.eu");

        driver.findElement(By.id("flight_form_firstName")).sendKeys("Jiri");
        driver.findElement(By.id("flight_form_lastName")).sendKeys("Sebek");
        driver.findElement(By.id("flight_form_birthDate")).sendKeys("01013000");
        driver.findElement(By.id("flight_form_email")).sendKeys("omo@fel.cz");
        driver.findElement(By.id("flight_form_discount_2")).click();
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination"))); selectDestination.selectByVisibleText("London");

        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();
    }
}
